package tk.jenkins.common;

class GitHelper {
  static def getCurrentBranch(pipeline) {
    def fullBranchName = pipeline.scm.branches[0].toString()
    return GitHelper.getShortBranchName(pipeline, fullBranchName)
  }

  static def getShortBranchName(pipeline, fullBranchName) {
    def m = fullBranchName =~ /(?:\*|origin|refs\/heads|refs\/tags|)\/?(?<branch>\S+)$/
    if (m.matches()) {
      return m.group('branch')
    } else {
      pipeline.error("Error. Unknown branch '${fullBranchName}' ?!")
    }
  }

  static def cloneRepositories(pipeline, repoFileYaml, dirName, credentialsId, gitBranch) {
    def data = pipeline.readYaml file: repoFileYaml

    pipeline.dir(dirName) {
      data.each { repoName, repoUrl ->
        pipeline.dir("${dirName}/${repoName}") {
          pipeline.git branch: gitBranch, credentialsId: credentialsId, url: repoUrl
        }
      }
    }
  }

  @NonCPS
  static def getChangeLogs(pipeline) {
    def changeLog = ''
    def changeLogSets = pipeline.currentBuild?.changeSets
    if (changeLogSets) {
      for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
          def entry = entries[j]
          changeLog = changeLog + "${entry.commitId} by ${entry.author} on ${new Date(entry.timestamp)}: ${entry.msg}\n"
        }
      }
    }
    return changeLog
  }
}
