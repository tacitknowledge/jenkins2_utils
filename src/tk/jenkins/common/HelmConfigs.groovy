package tk.jenkins.common;

class HelmConfigs {

  static def get(pipeline, configPath, application, type='') {
    def secretFileName = type ? "${type}-${application}" : application
    def secretsConfFile = "${configPath}/secrets.${secretFileName}.yaml"
    def confFile = "${configPath}/${application}.yaml"
    def args = ''
    [secretsConfFile, confFile].each { item ->
      if (pipeline.fileExists(item)) {
          args = args + " -f ${item}"
      }
    }
    return args
  }
}
