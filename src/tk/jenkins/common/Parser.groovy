package tk.jenkins.common;

import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput

class Parser {

  @NonCPS
  static parseJson(def json_string) {
    return new groovy.json.JsonSlurperClassic().parseText(json_string)
  }

  @NonCPS
  static jsonAsString(def jsonObject) {
    def json = JsonOutput.toJson(jsonObject)
    return JsonOutput.prettyPrint(json)
  }
}
