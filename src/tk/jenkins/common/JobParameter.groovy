package tk.jenkins.common;

class JobParameter {

  static def get(pipeline, paramName) {
      if (pipeline.params.containsKey(paramName)) {
        return pipeline.params.get(paramName);
      }
      pipeline.error "mandatory parameter '${paramName}' not defined!!!"
  }

  static def get(pipeline, paramName, defaultValue) {
      if (!pipeline.params.containsKey(paramName)) {
          return defaultValue;
      }

      return pipeline.params.get(paramName);
  }

  static def getBoolean(pipeline, paramName, defaultValue) {
      return JobParameter.get(pipeline, paramName, defaultValue).toBoolean();
  }
}
