package tk.jenkins.common;

class PodSecurityContext {

  static ROOT = """apiVersion: v1
kind: Pod
metadata:
spec:
  securityContext:
    runAsUser: 0
    fsGroup: 0"""

  static JENKINS_AGENT = """apiVersion: v1
kind: Pod
metadata:
spec:
  securityContext:
    runAsUser: 10000
    fsGroup: 10000"""

  static JENKINS_1000 = """apiVersion: v1
kind: Pod
metadata:
spec:
  securityContext:
    runAsUser: 1000
    fsGroup: 1000"""
}
