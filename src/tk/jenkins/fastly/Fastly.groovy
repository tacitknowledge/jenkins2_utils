package tk.jenkins.fastly

import groovy.json.JsonOutput
import tk.jenkins.common.Parser
import tk.jenkins.common.JobParameter


class Fastly {

  def fastlyToken
  def fastlyService
  def fastlyCredentialsIdName
  def p
  def acl_id
  def ip
  def subnet
  def acl_block_comments

  def init(service, token){
    this.fastlyService = service
    this.fastlyToken = token
  }
  
  def init(pipeline){
    this.p = pipeline
    this.fastlyService = JobParameter.get(p, 'fastlyService')
    this.fastlyCredentialsIdName = JobParameter.get(p, 'fastlyCredentialsIdName', 'fastly_token')

    p.withCredentials([p.string(credentialsId: fastlyCredentialsIdName, variable: 'fastly_token')]) {
        this.fastlyToken = p.fastly_token
    }
  }

  def getActiveVersion(){
    def versions = getRequest("/service/${fastlyService}/details")
    return Parser.parseJson(versions)['active_version']['number']
  }
  
  def activateVersion(version){
    return getRequest("/service/${fastlyService}/version/${version}/activate","PUT")
  }

  def purgeAll(){
    return getRequest("/service/${fastlyService}/purge_all","POST")
  }

  def addIpToACL(acl_id, ip, subnet, acl_block_comments){
    def body = [
      subnet: subnet,
      ip: ip,
      comment: acl_block_comments
    ] 
    return getRequest("/service/${fastlyService}/acl/${acl_id}/entry","POST", body)
  }

  private def createHttpRequest(uri,method, body = null) {
    def req = new URL("https://api.fastly.com${uri}").openConnection()
    req.setRequestProperty('Accept', 'application/json')
    req.setRequestProperty('Fastly-Key', "${fastlyToken}")
    req.setRequestMethod(method)
    if (body != null && method.toUpperCase() == 'POST') {
        req.setDoOutput(true)
        req.setRequestProperty('Content-Type', 'application/json')
        def outputStream = req.getOutputStream()
        outputStream.write(JsonOutput.toJson(body).getBytes('UTF-8'))
        outputStream.flush()
        outputStream.close()
    }

    return req
  }

  private def getRequest(uri,method="GET", body = null) {
    def req = createHttpRequest(uri,method, body)
    def respCode = req.getResponseCode()
    if (respCode.equals(200)) {
      return req.getInputStream().getText()
    }
    p.print req.getErrorStream().getText()
    throw new Exception("Fastly. invalid response ${respCode}")
  }
 
}