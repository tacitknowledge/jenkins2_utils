package tk.jenkins.newrelic;

import tk.jenkins.common.Parser

class SyntheticsMonitors {

  def baseURL = "https://synthetics.newrelic.com/synthetics/api/v3/monitors"
  def apiKey
  def p

  def init(pipeline, credentialsIdName){
    this.p = pipeline
    p.withCredentials([p.string(credentialsId: credentialsIdName, variable: 'apiKey')]) {
      this.apiKey = p.apiKey
    }
  }

  private def createHttpRequest(url, method="GET", message="") {
    def con = new URL(url).openConnection();
    con.setRequestMethod(method)
    con.setRequestProperty("X-Api-Key", apiKey)
    if ( method == "POST" ) {
      con.setRequestProperty("X-HTTP-Method-Override", "PATCH");
      con.setRequestProperty("Content-Type", "application/json")
      con.setDoOutput(true)
      con.getOutputStream().write(message.getBytes("UTF-8"));
    }
    def conRC = con.getResponseCode();
    if (conRC < 300) {
      return con.getInputStream().getText()
    }
    p.echo con.getErrorStream().getText()
    throw new Exception("Invalid response code ${conRC}")
   }

  def getMonitorList(select, contains) {
    def response
    def monitors
    def monitorsList = []
    response = createHttpRequest(baseURL)
    monitors = Parser.parseJson(response)['monitors']
    for (monitor in monitors) {
      if ( select == 'contains' ) {
        if ( monitor['name'].contains(contains) ) {
          monitorsList.add(monitor['id'])
        }
      } else {
        monitorsList.add(monitor['id'])
      }
    }
    return monitorsList
  }

  def syntheticsMonitorsPatch(select, contains, monitors, status) {
    def response
    def message = "{ \"status\" : \"$status\" }"

    if ( select == 'all' || select == 'contains' ) {
      monitors = getMonitorList(select, contains)
    }
    monitors.each {
      p.echo "Patching monitor $it"
      response = createHttpRequest("$baseURL/$it", "POST", message)
      p.echo "Monitor $it $status $response"
    }
  }

}
