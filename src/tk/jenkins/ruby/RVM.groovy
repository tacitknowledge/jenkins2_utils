package tk.jenkins.ruby;

import hudson.model.Result

class RVM {
    static def withRvm(pipeline, version, cl) {
        withRvm(pipeline, version, "executor-${pipeline.env.EXECUTOR_NUMBER}") {
            cl()
        }
    }

    static def withRvm(pipeline, version, gemset, cl) {
        pipeline.sh "echo ${pipeline.env.JOB_BASE_NAME}"
        def RVM_HOME = "${pipeline.env.HOME}/.rvm"
        def paths = [
                "$RVM_HOME/gems/$version@$gemset/bin",
                "$RVM_HOME/gems/$version@global/bin",
                "$RVM_HOME/rubies/$version/bin",
                "$RVM_HOME/bin",
                "${pipeline.env.PATH}"
        ]
        def path = paths.join(':')
        pipeline.withEnv(["PATH=${pipeline.env.PATH}:$RVM_HOME", "RVM_HOME=$RVM_HOME"]) {
            pipeline.sh "set +x; source $RVM_HOME/scripts/rvm; rvm use --create --install --binary $version@$gemset"
        }
        pipeline.withEnv([
                "PATH=$path",
                "GEM_HOME=$RVM_HOME/gems/$version@$gemset",
                "GEM_PATH=$RVM_HOME/gems/$version@$gemset:$RVM_HOME/gems/$version@global",
                "MY_RUBY_HOME=$RVM_HOME/rubies/$version",
                "IRBRC=$RVM_HOME/rubies/$version/.irbrc",
                "RUBY_VERSION=$version"
        ]) {
            cl()
        }
    }
}
