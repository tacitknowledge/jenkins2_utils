package tk.jenkins.http

class HttpRestClient {

    static final String ENCODING = 'UTF-8'
    public static final String POST = "POST"
    public static final String CONTENT_TYPE_HEADER = 'Content-Type'
    public static final String AUTHORIZATION_HEADER = 'Authorization'
    public static final String APPLICATION_JSON = 'application/json'

    def p
    def url
    def authorization

    HttpRestClient(pipeline, url, authorization) {
        this.p = pipeline
        this.url = url
        this.authorization = authorization
    }

    def getRequest(uri) {
        p.print "GET: ${url}${uri}"
        def req = createHttpRequest(uri)
        def respCode = req.getResponseCode()
        if (respCode.equals(200)) {
            return req.getInputStream().getText()
        }
        p.print req.getErrorStream().getText()
        throw new Exception("${url}${uri} invalid response ${respCode}")
    }

    def getRequest(uri, customHeaders) {
        p.print "GET: ${url}${uri}"
        def req = createHttpRequest(uri)
        if (customHeaders != null) {
            customHeaders.each { k, v ->
                req.setRequestProperty(k, v)
            }
        }
        def respCode = req.getResponseCode()
        if (respCode.equals(200)) {
            return req.getInputStream().getText()
        }
        p.print req.getErrorStream().getText()
        throw new Exception("${url}${uri} invalid response ${respCode}")
    }

    def postRequest(uri, data, customHeaders) {
        p.print "POST: ${url}${uri}"
        p.print "Payload: '${data}'"

        def req = createHttpRequest(uri)
        if (customHeaders != null) {
            customHeaders.each { k, v ->
                req.setRequestProperty(k, v)
            }
        }
        req.setRequestMethod(POST)
        req.setDoInput(true)
        req.setDoOutput(true)
        def bytesData = data.getBytes(ENCODING)
        req.outputStream << bytesData

        def respCode = req.getResponseCode()
        p.print "${url}${uri} Response code: ${respCode}"
        if (respCode.equals(200) || respCode.equals(201)) {
            return req.getInputStream().getText()
        } else if (!respCode.equals(204)) {
          p.print req.getErrorStream().getText()
          p.print req.getInputStream().getText()
          throw new Exception("${url}${uri} invalid response ${respCode}")
        }
    }

    private def createHttpRequest(uri) {
        def req = new URL("${url}${uri}").openConnection()
        req.setRequestProperty(CONTENT_TYPE_HEADER, APPLICATION_JSON)
        req.setRequestProperty(AUTHORIZATION_HEADER, authorization)
        return req
    }
}
