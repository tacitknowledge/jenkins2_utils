package tk.jenkins.pd

import java.util.Calendar
import groovy.json.JsonOutput

import tk.jenkins.common.JobParameter
import tk.jenkins.common.Parser

class PagerDuty implements Serializable {

  static final String DATE_FORMAT = 'yyyyMMdd-HH:mm:ssZ'
  static final String ENCODING = 'UTF-8'

  def p
  def credentialsIdName
  def notificationEmail
  def pagerService
  def pagerDutySecretKey

  def init(pipeline) {
    this.p = pipeline
    this.pagerService = JobParameter.get(p, 'pdService')
    this.credentialsIdName = JobParameter.get(p, 'pdCredentialsIdName', 'pager_key')
    this.notificationEmail = JobParameter.get(p, 'notificationEmail')

    p.withCredentials([p.string(credentialsId: credentialsIdName, variable: 'pager_key')]) {
        this.pagerDutySecretKey = p.pager_key
    }
  }

  /*
  * Create Pager Duty Maintenance Window
  */
  def create(duration, description) {
    return createOrUpdate(duration, description)
  }

  /*
  * Updates existing pager duty maintenance window
  */
  def update(windowId, duration, description) {
    if (windowId?.trim()) {
      return createOrUpdate(duration, description, windowId)
    }
    p.print "WARNING: windowId is null/empty. Can't update PD Maintenance Window"
    return null
  }

  def updateByDescription(description, duration) {
    def windows = listOngoingWindows()
    windows.each { w ->
      if ( w['summary'] == description) {
        def windowId = w['id']
        p.print "Extend Maintenance Window ${windowId} to more ${duration} minutes ..."
        createOrUpdate(duration, description, windowId)
      }
    }
  }

  def listAllWindows() {
    def response = getRequest("/maintenance_windows?service_ids%5B%5D=${pagerService}")
    return Parser.parseJson(response)['maintenance_windows']
  }

  def listOngoingWindows() {
    def response = getRequest("/maintenance_windows?service_ids%5B%5D=${pagerService}&filter=ongoing")
    return Parser.parseJson(response)['maintenance_windows']
  }

  def removeAllByDescription(description) {
    def windows = listOngoingWindows()
    windows.each { w ->
      if ( w['summary'] == description) {
        p.print removeById(w['id'])
      }
    }
  }

  def removeById(windowId) {
    p.print "Removing maintenance window ${windowId} ..."
    def response = deleteRequest("/maintenance_windows/${windowId}")
    return response
  }

  private def createOrUpdate(duration, description, windowId = null) {
    try {
        return createOrUpdateWindow(windowId, duration, description)
    } catch (any) {
      p.print "PAGERDUTY call failed: ${any}"
      return null
    }
  }

  private def createOrUpdateWindow(windowId, duration, description) {

    def currentTimeMs = Calendar.getInstance().getTimeInMillis()
    def now = new Date(currentTimeMs)
    def endTime = new Date(currentTimeMs + (duration as int) * 60000)

    def requestMap = [maintenance_window: [
      type: 'maintenance_window',
      start_time: now.format(DATE_FORMAT),
      end_time: endTime.format(DATE_FORMAT),
      description: description,
      services: [[ id: this.pagerService, type: 'service_reference']]
    ]]

    if (windowId?.trim()) {
      requestMap['maintenance_window'].remove('start_time')
      p.print "Updating Maintenance Window ${windowId} ..."
    } else {
      p.print "Creating new Maintenance Window ..."
    }

    def data = JsonOutput.toJson(requestMap)

    def uri = '/maintenance_windows'
    def method = windowId == null ? 'POST' : 'PUT'
    def customHeaders = windowId == null ? ['From': notificationEmail] : null

    if (windowId != null) {
      uri += '/' + windowId
    }

    def stringResponse = postRequest(uri, data, method, customHeaders)
    def jsonResponse = Parser.parseJson(stringResponse)
    return jsonResponse['maintenance_window']['id']
  }

  private def createHttpRequest(uri) {
    def req = new URL("https://api.pagerduty.com${uri}").openConnection()
    //def req = new URL("http://10.8.0.111:3333${uri}").openConnection()
    req.setRequestProperty('Accept', 'application/vnd.pagerduty+json;version=2')
    req.setRequestProperty('Authorization', "Token token=${pagerDutySecretKey}")
    return req
  }

  private def getRequest(uri) {
    def req = createHttpRequest(uri)
    def respCode = req.getResponseCode()
    if (respCode.equals(200)) {
      return req.getInputStream().getText()
    }
    p.print req.getErrorStream().getText()
    throw new Exception("PD. invalid response ${respCode}")
  }

  private def deleteRequest(uri) {
    def req = createHttpRequest(uri)
    req.setRequestMethod('DELETE')
    def respCode = req.getResponseCode()
    p.print "PagerDuty Response code: ${respCode}"
    if (respCode.equals(204)) {
      return req.getInputStream().getText()
    }

    p.print req.getErrorStream().getText()
    throw new Exception("PD. invalid response ${respCode}")
  }

  private def postRequest(uri, data, method, customHeaders) {
    p.print "${method} request to '${uri}'"
    p.print "Sending '${data}' ..."

    def req = createHttpRequest(uri)
    if (customHeaders != null) {
      customHeaders.each { k,v ->
        req.setRequestProperty(k, v)
      }
    }
    req.setRequestMethod(method)
    req.setDoInput(true)
    req.setDoOutput(true)
    req.setRequestProperty('Content-Type', 'application/json')
    def bytesData = data.getBytes(ENCODING)
    req.outputStream << bytesData

    def respCode = req.getResponseCode()
    p.print "PagerDuty Response code: ${respCode}"
    if (respCode.equals(200) || respCode.equals(201)) {
      return req.getInputStream().getText()
    }

    p.print req.getErrorStream().getText()
    p.print req.getInputStream().getText()
    throw new Exception("PD. invalid response ${respCode}")
  }
}
