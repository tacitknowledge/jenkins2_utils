package tk.jenkins.jira

import groovy.json.JsonOutput
import tk.jenkins.common.Parser
import tk.jenkins.http.HttpRestClient

class Jira {

    def p
    def httpRestClient

    Jira(pipeline, apiToken, jiraHost, jiraEntityID) {
        this.p = pipeline
        this.httpRestClient = new HttpRestClient(pipeline,
                "https://${jiraHost}/rest/api/2/issue/${jiraEntityID}",
                "Basic ${apiToken}")
    }

    def transition(transitionId) {
        def requestDataMap = [
                transition     : [id : transitionId]
        ]
        httpRestClient.postRequest("/transitions", JsonOutput.toJson(requestDataMap), null)
    }
}
