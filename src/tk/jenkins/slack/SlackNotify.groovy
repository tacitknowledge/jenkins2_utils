package tk.jenkins.slack;

import hudson.model.Result

class SlackNotify {

  static def notifySlackCommon(pipeline, channel, messageText='', color = 'danger', icon=""){
    def message = messageText + icon + '\n' + pipeline.env.BUILD_URL + '\n' + " *${pipeline.env.JOB_BASE_NAME}* build ${pipeline.currentBuild.displayName.replaceAll("#", "")}"
    pipeline.slackSend channel: "${channel}", color: "${color}", message: "${message}"
  }

  static def notifySlack(pipeline, channel, sendAlways=false, messageText='') {
    def buildStatus =  pipeline.currentBuild.result ? pipeline.currentBuild.result : 'SUCCESS'
    def message = " *${pipeline.env.JOB_BASE_NAME}* build ${pipeline.currentBuild.displayName.replaceAll("#", "")}"
    def status = ''
    def icon = ''
    def color = 'good'
    def sendMessage = false

    switch (buildStatus) {
      case 'SUCCESS':
        def prevResult = pipeline.currentBuild.getPreviousBuild() != null ? pipeline.currentBuild.getPreviousBuild().getResult() : null;
        if (Result.FAILURE.toString().equals(prevResult) || Result.UNSTABLE.toString().equals(prevResult)) {
          status = " *FIXED* "
          icon = ":white_check_mark:"
          sendMessage = true
          break
        }
        status += " *SUCCESSFUL* "
        icon = ":white_check_mark:"
        break
      case 'FAILURE':
        status = " *FAILED* "
        icon = ":evil-jenkins:"
        color = 'danger'
        sendMessage = true
        break
      case 'UNSTABLE':
        status += " *UNSTABLE* "
        icon = ":eight_pointed_black_star:"
        color = 'warning'
        break
      case 'ABORTED':
        status += " *ABORTED* "
        icon = ":eight_pointed_black_star:"
        break
      case 'NOT_BUILT':
        status += " *NOT BUILT* "
        icon = ":eight_pointed_black_star:"
        break
      case 'FIXED':
        status = " *FIXED* "
        icon = ":white_check_mark:"
        sendMessage = true
        break
    }

    message = icon + message + status + pipeline.env.BUILD_URL + '\n' + messageText

    if (sendAlways || sendMessage) {
      pipeline.slackSend channel: "${channel}", color: "${color}", message: "${message}"
    }
  }
}
