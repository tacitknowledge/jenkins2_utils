package tk.jenkins.azure;

import groovy.json.JsonSlurper

class AzureContainerRegistry {
    static def getTags(pipeline, registryName, repositoryName, excludeTags = [], sortAsNumbers = false) {
        def stdout = pipeline.sh(
                script: "az acr repository show-tags --name ${registryName} --repository ${repositoryName}",
                returnStdout: true)

        def tags = new JsonSlurper().parseText(stdout).flatten()

        excludeTags.each {
            tags = tags - it
        }

        if (sortAsNumbers) {
            tags.sort { a, b ->
                return b as Integer <=> a as Integer
            }
        } else {
            tags.sort()
            tags = tags.reverse()
        }

        tags
    }

    static def filterTags(pipeline, tags, suffix) {
        List filteredtags = tags.findAll { it =~ /(?i).*[0-9]$suffix$/ }

        if (filteredtags.isEmpty()) {
            pipeline.error 'No valid artifacts to deploy, make sure the tag is valid'
        }

        filteredtags
    }

    static def selectVersionForDeployment(pipeline, tags, showMaxItems = 20) {
        def version = tags[0]

        if (pipeline.currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause)) {
            pipeline.echo 'Requesting user to set version'
            if (tags.size() > showMaxItems) {
              tags = tags[0..showMaxItems-1]
              pipeline.echo "Show latest ${showMaxItems} versions"
            }
            version = pipeline.input(
                    id: 'version',
                    message: 'Promote:',
                    parameters: [
                            [$class     : 'ChoiceParameterDefinition',
                             choices    : tags.join("\n"),
                             description: 'Versions',
                             name       : 'version']])
        }

        pipeline.echo "Selected version is: ${version}"
        version
    }

    static Boolean approveStableBuild(pipeline, timeoutInSec) {
        Boolean approve
        pipeline.echo 'Requesting user to approve build'

        pipeline.timeout(time: timeoutInSec, unit: 'SECONDS') {
            approve = pipeline.input(
                    id: 'isStable',
                    message: 'Is this build Stable?',
                    parameters: [
                            [$class      : 'BooleanParameterDefinition',
                             defaultValue: false,
                             description : 'Build will be tagged as _APPROVED if approved',
                             name        : 'Approve?']])
        }

        approve
    }

    static def cleanOldImages(pipeline, registryName, repositoryName, keep = 5, excludeTags = ['latest', 'stable']) {
        def tags = getTags(pipeline, registryName, repositoryName, excludeTags)
        if (tags.size() <= keep) {
            pipeline.echo 'No tags for cleanup'
        } else {
            def tagsForCleanup = tags.drop(keep)
            pipeline.echo "Deleting ${tagsForCleanup.size()} tags"
            for (String tag : tagsForCleanup) {
                pipeline.sh "az acr repository delete --name ${registryName} --image ${repositoryName}:${tag} --yes"
            }
        }
    }
}
