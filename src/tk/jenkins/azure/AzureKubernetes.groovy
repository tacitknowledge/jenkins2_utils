package tk.jenkins.azure;

class AzureKubernetes {

  static getCredentials(pipeline, cluster, resourceGroup) {
    pipeline.sh(returnStatus: true, script: 'rm -rf ~/.kube')
    pipeline.sh "az aks get-credentials --resource-group ${resourceGroup} --name ${cluster}"
  }

  static getAllPods(pipeline, namespace) {
    def pods = pipeline.sh(returnStdout: true, script: """kubectl get pods -n ${namespace} -o=jsonpath='{.items[*].metadata.name}'""").split()
    return pods
  }

  static deletePod(pipeline, namespace, pod) {
    try {
        String result = pipeline.sh(script: "kubectl delete -n ${namespace} pod ${pod}", returnStdout: true).trim()
        pipeline.echo("Pod deletion successful: ${result}")
    } catch (Exception e) {
        pipeline.echo("Failed to delete pod: ${e.message}")
        throw e
    }
  }
}