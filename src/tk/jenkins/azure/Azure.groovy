package tk.jenkins.azure;

class Azure {

  static login(pipeline, username, password,tenant,subscription) {
    pipeline.sh "az login --service-principal -u '${username}' -p '${password}' --tenant ${tenant}"
    pipeline.sh "az account set --subscription ${subscription}"
  }
}
