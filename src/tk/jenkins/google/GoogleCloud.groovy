package tk.jenkins.google;

import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput

class GoogleCloud {
  static final HEADERS = "Metadata-Flavor: Google"
  static final METADATA_URL = "http://metadata/computeMetadata"
  static final EXTERNAL_IP_URL = METADATA_URL + "/v1/instance/network-interfaces/0/access-configs/0/external-ip"

  static def getExternalIp(pipeline) {
    return pipeline.sh(
      returnStdout: true,
      script: "curl -H \"${HEADERS}\" \"${EXTERNAL_IP_URL}\""
    )
  }

  static def cloudSdkLogin(pipeline,serviceAccountKey){
    pipeline.sh "gcloud auth activate-service-account --key-file ${serviceAccountKey}"
  }

  static def kubectlApply(pipeline, stringContent) {
    def randomUid = UUID.randomUUID().toString()
    def tempFileName = "${randomUid}.json"
    pipeline.writeFile(file:tempFileName, text: stringContent)
    try {
      pipeline.sh("kubectl apply -f ${tempFileName}")
    } catch (any) {
      pipeline.sh("cat ${tempFileName}")
      throw any //rethrow exception to prevent the build from proceeding
    } finally {
      //TODO: Do we have pipeline native method for this?
      pipeline.sh("rm -f ${tempFileName}")
    }
  }

  static def isFirewallOpened(pipeline, svcName, environment, myip) {
    def svc_definition = pipeline.sh(
      script: "kubectl get svc/${svcName} -n ${environment} -o json", returnStdout: true)
    def svc = new JsonSlurperClassic().parseText(svc_definition)
    svc.spec.loadBalancerSourceRanges.find { it == "${myip}/32" } != null
  }

  static def firewallOpen(pipeline, svcName, environment, myip) {
    pipeline.echo("firewallOpen service: ${svcName}, env: '${environment}', myIP: ${myip}")
    def svc_definition = pipeline.sh(
        script: "kubectl get svc/${svcName} -n ${environment} -o json", returnStdout: true)
    def svc = new JsonSlurperClassic().parseText(svc_definition)
    if (svc.spec.loadBalancerSourceRanges == null) {
      pipeline.echo("service '${svcName}' is already accessible from any IP. Ignoring")
      return
    }

    if (svc.spec.loadBalancerSourceRanges.find { it == "${myip}/32" }) {
        pipeline.echo("ip ${myip} already in whitelist - ignoring")
    } else {
        svc.spec.loadBalancerSourceRanges.add("${myip}/32")
        def new_svc_definition = JsonOutput.prettyPrint(JsonOutput.toJson(svc))
        GoogleCloud.kubectlApply(pipeline, new_svc_definition)

        pipeline.timeout(20) {
            pipeline.waitUntil {
              sleep 10
              isFirewallOpened(pipeline, svcName, environment, myip)
            }
        }
    }
  }

  static def firewallClose(pipeline, svcName, environment, myip) {
    pipeline.echo("firewallClose service: ${svcName}, env: '${environment}', myIP: ${myip}")
    def svc_definition = pipeline.sh(
        script: "kubectl get svc/${svcName} -n ${environment} -o json", returnStdout: true)
    def svc = new JsonSlurperClassic().parseText(svc_definition)
    if (svc.spec.loadBalancerSourceRanges == null) {
      pipeline.echo("service '${svcName}' is accessible from any IP. Ignoring close request")
      return
    }

    if (svc.spec.loadBalancerSourceRanges.removeAll { it == "${myip}/32" }) {
        def new_svc_definition = JsonOutput.prettyPrint(JsonOutput.toJson(svc))
        GoogleCloud.kubectlApply(pipeline, new_svc_definition)
    } else {
        pipeline.echo("ip ${myip} not in whitelist - ignoring")
    }
  }
}
