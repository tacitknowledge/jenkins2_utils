package tk.jenkins.google;

import groovy.json.JsonSlurper

class GoogleCloudRegistry {

  static def getTags(pipeline, gcrRepo, removeTags = [], sortAsNumbers = false) {
    def stdout = pipeline.sh(
      script: "gcloud container images list-tags ${gcrRepo} --format json",
      returnStdout: true)

    def tags = new JsonSlurper().parseText(stdout)['tags'].flatten()

    removeTags.each {
      tags = tags - it
    }

    if (sortAsNumbers) {
      tags.sort{ a,b ->
        return b as Integer <=> a as Integer
      }
    } else {
      tags.sort()
      tags = tags.reverse()
    }

    return tags
  }

  static def selectVersionForDeployment(pipeline, tags, showMaxItems = 20) {
    def version

    if (pipeline.currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause)) {
      pipeline.echo 'Requesting user to set version'
      if (tags.size() > showMaxItems) {
        tags = tags[0..showMaxItems-1]
        pipeline.echo "Show latest ${showMaxItems} versions"
      }
      version = pipeline.input(
        id: 'version',
        message: 'Promote:',
        parameters: [
          [ $class: 'ChoiceParameterDefinition',
            choices: tags.join("\n"),
            description: 'Versions',
            name: 'version' ]])
    } else {
      version = tags[0]
    }

    pipeline.echo "Deploying version: ${version}"
    return version
  }

    static def deleteImage(pipeline, image) {
    pipeline.echo "Deleting ${image}"
    pipeline.sh "gcloud container images delete --force-delete-tags --quiet ${image}"
  }

  static def getOldTags(pipeline, gcrRepo, keep = 5) {
    def tags = getTags(pipeline, gcrRepo, ['latest', 'stable'])
    def oldTags = []
    if (tags.size() <= keep) {
      pipeline.echo 'No tags for cleanup'
    } else {
      oldTags = tags.drop(keep)
    }
    return oldTags
  }

  static def cleanOldImages(pipeline, gcrRepo, keep = 5) {
      def tagsForCleanup = getOldTags(pipeline, gcrRepo, keep)
      if (tagsForCleanup) {
        pipeline.echo "Deleting ${tagsForCleanup.size()} tags"
        def tagsAsString = tagsForCleanup.collect { "${gcrRepo}:$it" }.join(' ')
        deleteImage(pipeline, tagsAsString)
      }
  }
}
