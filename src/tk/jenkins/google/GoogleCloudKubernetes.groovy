package tk.jenkins.google

class GoogleCloudKubernetes {

  static getCredentials(pipeline, cluster, zone, project) {
    pipeline.sh(returnStatus: true, script: 'rm -rf ~/.kube')
    pipeline.sh "gcloud container clusters get-credentials ${cluster} --zone ${zone} --project ${project}"
  }

  static imageIsUsed(pipeline, image) {
    def isPresent = false
    def objects = ['deployment', 'statefulsets', 'daemonsets', 'pods']
    for (def it: objects) {
      def images = pipeline.sh(returnStdout: true, script: """kubectl get ${it} -A -o jsonpath="{..image}" """)
      if (images.contains(image)) {
        isPresent = true
        break
      }
    }
    return isPresent
  }

  static getPods(pipeline, namespace, labels='app.kubernetes.io/name=hybris') {
    def pods = pipeline.sh(returnStdout: true, script: """kubectl get pods -n ${namespace} -l ${labels} -o=jsonpath='{.items[*].metadata.name}'""").split()
    return pods
  }

  static getAllPods(pipeline, namespace) {
    def pods = pipeline.sh(returnStdout: true, script: """kubectl get pods -n ${namespace} -o=jsonpath='{.items[*].metadata.name}'""").split()
    return pods
  }

  static deletePod(pipeline, namespace, pod) {
    try {
        String result = pipeline.sh(script: "kubectl delete -n ${namespace} pod ${pod}", returnStdout: true).trim()
        pipeline.echo("Pod deletion successful: ${result}")
    } catch (Exception e) {
        pipeline.echo("Failed to delete pod: ${e.message}")
        throw e
    }
  }

  static runCommandInContainer(pipeline, namespace, pod, command, container='hybris') {
    return pipeline.sh(script:"""kubectl exec -it -n ${namespace} -c ${container} ${pod} -- /bin/bash -c "${command}" """, returnStdout: true).trim()
  }

  static generateFileName(pod, type) {
    def now = new Date().format('YYYY-MM-dd-HH-mm-ss')
    def fileName = "${pod}-${now}.${type}"
    return fileName
  }
  static getThreadDump(pipeline, namespace, pod, uploadUrl, fileName, dumpsLocation='/opt/hybris/dumps') {
      def pid = runCommandInContainer(pipeline, namespace, pod, "pgrep -f '[H]YBRIS_DATA_DIR'")
      runCommandInContainer(pipeline, namespace, pod, "mkdir -p ${dumpsLocation}")
      def createDumpCmd = "jstack -l ${pid} >> ${dumpsLocation}/${fileName} && gzip -9 ${dumpsLocation}/${fileName}"
      runCommandInContainer(pipeline, namespace, pod, createDumpCmd)
      def uploadDumpCmd = "curl --silent --request PUT --upload-file ${dumpsLocation}/${fileName}.gz '${uploadUrl}'"
      runCommandInContainer(pipeline, namespace, pod, uploadDumpCmd)
  }

  static getHeapDump(pipeline, namespace, pod, uploadUrl, fileName, dumpsLocation='/opt/hybris/dumps') {
      def pid = runCommandInContainer(pipeline, namespace, pod, "pgrep -f '[H]YBRIS_DATA_DIR'")
      runCommandInContainer(pipeline, namespace, pod, "mkdir -p ${dumpsLocation}")
      def createDumpCmd = "jmap -dump:format=b,file=${dumpsLocation}/${fileName} ${pid} && gzip ${dumpsLocation}/${fileName} && chmod +r ${dumpsLocation}/${fileName}.gz"
      runCommandInContainer(pipeline, namespace, pod, createDumpCmd)
      def uploadDumpCmd = "curl --silent --request PUT --upload-file ${dumpsLocation}/${fileName}.gz '${uploadUrl}'"
      runCommandInContainer(pipeline, namespace, pod, uploadDumpCmd)
  }

  static getResumableUrl(
    pipeline,
    serviceAccount,
    bucketName,
    region,
    fileName,
    contentType='application/octet-stream',
    duration='10m') {
    def signedUrl = pipeline.sh(script: """
      gsutil signurl \
          -c '${contentType}' \
          -d ${duration} \
          -m RESUMABLE \
          -r ${region} \
          "${serviceAccount}" \
          gs://${bucketName}/${fileName} """, returnStdout: true
      ).trim().split().last()
    println signedUrl
    return pipeline.sh(script: """
      curl --verbose \
          --location \
          --dump-header - \
          --request 'POST' \
          --header 'content-type: ${contentType}' \
          --header 'x-goog-resumable:start' \
          --data '' '${signedUrl}' | grep location""", returnStdout: true
      ).trim().split().last()
    }

}
