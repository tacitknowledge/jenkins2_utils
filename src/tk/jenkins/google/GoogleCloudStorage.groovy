package tk.jenkins.google;

class GoogleCloudStorage{

  static def syncBuckets(pipeline,sourceBucket,destinationBucket,cleanupDestination=true,syncBucketsArgs=''){
    def args = '-r'
    if (cleanupDestination){
      args += ' -d'
    }
    if (syncBucketsArgs){
      args += " ${syncBucketsArgs}"
    }
    pipeline.sh("gsutil -q -m rsync ${args} ${sourceBucket} ${destinationBucket}")
  }

}
