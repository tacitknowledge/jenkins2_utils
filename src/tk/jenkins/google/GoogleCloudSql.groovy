package tk.jenkins.google;

class GoogleCloudSql {

  static def cloudSqlExport(pipeline,instance,database,bucket,exportName=database,timeout=3600){
    if (cloudSqlGetRunningOperations(pipeline,instance)){
      throw new Exception("Other operation is in progress, please try later")
    }
    def operation = pipeline.sh (returnStdout: true, script:"gcloud sql export sql ${instance} --database ${database} ${bucket}/${exportName}.sql.gz --async --format='value(selfLink)'").trim()
    cloudSqlWaitOperationToComplete(pipeline,instance,operation,timeout)
  }

  static def cloudSdkLogin(pipeline,serviceAccountKey){
    pipeline.sh "gcloud auth activate-service-account --key-file ${serviceAccountKey}"
  }

  static def cloudSqlGetRunningOperations(pipeline,instance){
    if (pipeline.sh(returnStdout: true, script:"gcloud sql operations list --instance ${instance} --filter='status!=DONE' --format='value(selfLink)'")) {
      return true
    } else {
      return false
    }
  }

  static def cloudSqlWaitOperationToComplete(pipeline,instance,operation,timeout=3600){
    pipeline.sh """gcloud sql operations wait --timeout "${timeout}" --quiet "$operation" """
    if(pipeline.sh(returnStdout: true, script: """gcloud sql operations list --instance="${instance}" --filter="status=DONE selfLink=${operation}" --format='value(NAME)'""")){
        pipeline.echo "${operation} is done"
    } else {
        throw new Exception("Something went wrong!")
    }
  }

  static def cloudSqlImport(pipeline,instance,database,uri,timeout=3600){
    if (cloudSqlGetRunningOperations(pipeline,instance)){
      throw new Exception("Other operation is in progress, please try later")
    }
    def operation = pipeline.sh(returnStdout: true, script:"gcloud sql import sql ${instance} --database ${database} ${uri} --async --format='value(selfLink)'").trim()
    cloudSqlWaitOperationToComplete(pipeline,instance,operation,timeout)
  }

  static def removeUseDatabaseRow(pipeline,database,bucket,file="${database}.sql.gz"){
    pipeline.sh """gsutil -q -m cp ${bucket}/${file} ./
                   gunzip ${file}
                   split -l 10000 ${database}.sql "chunk" """
    def files = pipeline.sh(returnStdout: true, script:"ls chunk*").trim().split()
    for(chunk in files){
        pipeline.sh "sed -i '/${database}/d' ./${chunk}"
    }
    pipeline.sh """cat chunk* > ${database}.sql
                   gzip ${database}.sql
                   gsutil -m cp ${file}  ${bucket}/${file}"""
  }
  
}