package tk.jenkins.ccv2

import groovy.json.JsonOutput
import tk.jenkins.common.Parser
import tk.jenkins.http.HttpRestClient

class CCv2 {

    def p
    def gitTag
    def buildCode
    def deploymentCode
    def httpRestClient

    CCv2(pipeline, apiToken, subscription, gitTag) {
        this.p = pipeline
        this.gitTag = gitTag
        this.httpRestClient = new HttpRestClient(pipeline,
                "https://portalrotapi.hana.ondemand.com/v2/subscriptions/${subscription}",
                "Bearer ${apiToken}")
    }

    def startBuild() {
        def requestDataMap = [
                applicationCode: 'commerce-cloud',
                branch         : gitTag,
                name           : getBuildName()
        ]
        buildCode = Parser.parseJson(httpRestClient.postRequest("/builds", JsonOutput.toJson(requestDataMap), null))['code']
    }

    def startDeployment(updateMode, strategy, environment) {
        def requestDataMap = [
                buildCode         : getBuildCode(),
                databaseUpdateMode: updateMode,
                environmentCode   : environment,
                strategy          : strategy
        ]
        deploymentCode = Parser.parseJson(httpRestClient.postRequest("/deployments", JsonOutput.toJson(requestDataMap), null))['code']
    }

    def getBuildCode() {
        p.print "GET buildCode for tag ${gitTag}"
        buildCode != null ?
                buildCode :
                Parser
                        .parseJson(httpRestClient.getRequest("/builds"))
                        .get("value")
                        .find {
                            build -> build.name == getBuildName()
                        }.code
    }

    def getBuildStatus() {
        try {
            Parser.parseJson(httpRestClient.getRequest("/builds/${buildCode}"))['status']
        } catch (err) {
            return err.toString()
        }
    }

    def getDeploymentStatus() {
        try {
            Parser.parseJson(httpRestClient.getRequest("/deployments/${deploymentCode}"))['status']
        } catch (err) {
            return err.toString()
        }
    }

    /**
     * Return a valid CCv2 Build name. CCv2 build name do not allow the '/' char in their names,
     * hence raplacing them with a '-'
     */
    def private getBuildName() {
        gitTag.replaceAll("/", "-")
    }
}
