package tk.jenkins.opensearch

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import tk.jenkins.http.HttpRestClient

class OpenSearch {
    def p
    String username
    String password
    String host
    String tenant
    HttpRestClient httpClient
    Map<String, String> headers

    def init(pipeline, host, tenant, credentialsId) {
        this.p = pipeline
        this.host = host
        this.tenant = tenant

        p.withCredentials([p.usernamePassword(credentialsId: credentialsId, passwordVariable: 'opensearch_password', usernameVariable: 'opensearch_username')]) {
            this.username = p.opensearch_username
            this.password = p.opensearch_password
        }

        this.headers = [
            "osd-xsrf" : "true",
            "securitytenant" : "${this.tenant}"
        ]

        def credentials = "${this.username}:${this.password}"
        def encodedCredentials = Base64.getEncoder().encodeToString(credentials.getBytes())
        this.httpClient = new HttpRestClient(pipeline, "https://${host}", "Basic ${encodedCredentials.toString()}")
    }

    def export(type, id) {
        def requestDataMap = [
            "objects": [
                [
                    "type": type,
                    "id": id
                ]
            ],
            "includeReferencesDeep": true
        ]

        return this.httpClient.postRequest("/api/saved_objects/_export", JsonOutput.toJson(requestDataMap), this.headers)
    }

    def getObject(type, id) {
        def response = this.httpClient.getRequest("/api/saved_objects/$type/$id", this.headers)
        def jsonSlurper = new JsonSlurper()
        def object = jsonSlurper.parseText(response)

        return [
            "id": object.id,
            "type": object.type,
            "title": object.attributes.title
        ]
    }

    def listObjects(type) {
        def response = this.httpClient.getRequest("/api/saved_objects/_find?type=$type", this.headers)
        def jsonSlurper = new JsonSlurper()
        def objects = jsonSlurper.parseText(response)

        def objectList = []
        for(o in objects.saved_objects) {
            objectList.push([
                "id": o.id,
                "type": o.type,
                "title": o.attributes.title
            ])
        }

        return objectList
    }
}

