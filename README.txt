Common code library for jenkins2 (to be used from pipelines)

**Importing into your jenkins**:
Manage Jenkins => Configure System => Global Pipeline Libraries
Add new library with:
name = tk_jenkins2_utils
Default version = master
Project Repository = https://bitbucket.org/tacitknowledge/jenkins2_utils.git


**Example of usage - default master branch**:
```
@Library('tk_jenkins2_utils') import tk.jenkins.common.*

if (JobParameter.getBoolean(this, 'isZuzuEnabled', false)) {
  ...
}
```
**Example of usage - testing a feature branch**:
```
@Library('tk_jenkins2_utils@feature/XYZ') import tk.jenkins.common.*

if (JobParameter.getBoolean(this, 'isGigiDisabled', false)) {
  ...
}
```

### How to configure Slack plugin in Jenkins https://github.com/jenkinsci/slack-plugin

1. Get a Slack account: https://slack.com/
2. Configure the Jenkins integration: https://my.slack.com/services/new/jenkins-ci
3. Install plugin on Jenkins server:
Manage Jenkins => Manage Plugins => Available
Search for slack and check the box next to install.
4. Use Jenkins Credentials and a credential ID to configure the Slack integration token.
Create a new Secret text credential
Select that credential as the value for the Integration Token Credential ID field
5. Configure Global Slack Notifier Settings
Manage Jenkins => Configure System => Global Slack Notifier Settings
Slack compatible app URL = https://tacitknowledge.slack.com/services/hooks/jenkins-ci/
Integration Token Credential ID = select the one configured in p.4
To test, one may put a Channel or Slack ID, but it is presumed that in the pipeline channel is given as a parameter

**Example of usage**:
```
@Library('tkutils') import static tk.jenkins.slack.SlackNotify.*

notifySlack(this, 'channel-name', true)
//the 3rd argument enables send notifications for every build, if false (default if missing) sends only FIXED and FAILED notifications
```

### How to use the plugin for CCv2 APIs in Jenkins pipelines

CCv2 are gradually releasing official API to be used for automating builds and deploys in CCv2.
At the time of this writing only Build APIs are available https://help.sap.com/viewer/452dcbb0e00f47e88a69cdaeb87a925d/SHIP/en-US/9d7854f5297a400e9c8ce3a2819d3c82.html
1. Obtain CCv2 API key - e.g. env.SECRET_API_TOKEN
2. Know your subscription ID - e.g. env.SECRET_SUBSCRIPTION
3. Typically, you will have run the code quality tools, unit tests, jacoco etc prior to attempt a build in CCv2 and then tag the "good commits"
The git tag can then be used to trigger a CCv2 build on the same code, e.g. ccv2-${env.TAG}

**Example of usage in Jenkinsfile**:
```
#!/usr/bin/env groovy
@Library('utils')
import tk.jenkins.ccv2.*
...
stage('Trigger CCv2 Build') {
    def build = new CCv2(this, env.SECRET_API_TOKEN, env.SECRET_SUBSCRIPTION, "ccv2-${env.TAG}")
    build.startBuild()
    buildStatus = build.getBuildStatus()
    echo "Build ${build.buildCode} Status ${buildStatus}"
    while(!"SUCCESS.equalsIgnoreCase(buildStatus)){
        echo "Build ${build.buildCode} Status ${buildStatus}"
        sleep(60)
        buildStatus = build.getBuildStatus()
        if (("FAIL).equalsIgnoreCase(buildStatus)) {
            throw new Exception("CCv2 Build FAILED")
        }
    }
}

stage('Trigger CCv2 Deployment') {
    def ccv2 = new CCv2(this, env.SECRET_API_TOKEN, env.SECRET_SUBSCRIPTION, "ccv2-${env.TAG}")
    ccv2.startDeployment(env.MIGRATIONMODE, env.DEPLOYMENTMODE, env.ENV)
    def deployStatus = ccv2.getDeploymentStatus()
    while (!"DEPLOYED".equalsIgnoreCase(deployStatus)) {
        echo "Deployment to $ENV : Status ${deployStatus}"
        sleep(90)
        deployStatus = ccv2.getDeploymentStatus()
        if ("FAIL".equalsIgnoreCase(deployStatus)) {
            throw new Exception("CCv2 Deployment FAILED")
        }
    }
}

...
```

### How to use the Jira utility

The Jira utility allows one to access the Jira REST APIs, see
https://developer.atlassian.com/server/jira/platform/rest-apis/ and
https://developer.atlassian.com/server/jira/platform/jira-rest-api-examples/

E.g.: This can be used to change the status of a Jira entity. Consider an example where Jenkins job executes an automation suite and the suite will pass or fail.
In this case, you may want to move the associated Jira either to "Ready for QA" or back into "Rework Required" status.
You will need to know the ID of the transition you want to move the jira entity into. You can get that using this CURL as example:
```
curl -X GET -u <jira_user>:<password> -H "Content-Type: application/json"  https://<jira_host>/rest/api/2/issue/<jira_key>/transitions
```
Pipeline usage example:
```
...
def transitionsMap = [
  "Ready for QA" : 151,
  "Rework Required" :271
]

stage('Jira Status Update') {
    if (isDeployableBranch()) {
        jira = new Jira(this, env.JENKINS_SECRET_API_TOKEN, 'jira.tacitknowledge.com', getJiraId())
        if (currentBuild.currentResult == 'SUCCESS') {
            jira.transition(transitionsMap["Ready for QA"])
        } else if (currentBuild.currentResult == 'FAILURE') {
            jira.transition(transitionsMap["Rework Required"])
        }
    }
}
...
```

### How to use the Newrelic utility

The Newrelic utility allows one to access the Newrelic REST APIs, see
https://docs.newrelic.com/docs/apis/synthetics-rest-api/monitor-examples/manage-synthetics-monitors-rest-api

E.g.: This can be used to change the status of Synthetics monitor. You can do that using this CURL as example:
```
curl -v -X PATCH -H 'X-Api-Key:<apiKey>' -H 'Content-Type: application/json' https://synthetics.newrelic.com/synthetics/api/v3/monitors/<monitorID> -d '{ "status" : "DISABLED" }'
```
Pipeline usage example:
```
...
def monitorsList = monitors.split(',')

stage('Set status') {
  def sm = new SyntheticsMonitors()
  sm.init(this, credentialsIdName)
  sm.syntheticsMonitorsPatch(select, contains, monitorsList, status)
}
...
```

### How to create GCP Cloud SQL database export/import
All GoogleCloudSql class methods should be executed in container with Google cloudsdk preinstalled

pipeline usage example:
```
...
stage('Create database export'){
    container('cloudsdk'){
        cloudSdkLogin(this,<path to SA key file>)
        #cloudSqlExport(this,<cloud sql intance name>,<database name>,<destination bucket path>)
        cloudSqlExport(this,'dev-instance','smoke_hybris_db','gs://sofology-qa-dev-bucket')
    }
}
stage('Remove use database command'){
    container('cloudsdk'){
        cloudSdkLogin(this,<path to SA key file>)
        #removeUseDatabaseRow(this,<source database name>,<remote bucket>)
        removeUseDatabaseRow(this, 'smoke_hybris_db', 'gs://sofology-qa-dev-bucket')
    }
}
stage('Create database import'){
    container('cloudsdk'){
        cloudSdkLogin(this,<path to SA key file>)
        #cloudSqlExport(this,<cloud sql intance name>,<database name>,<full url for backup file in bucket>)
        cloudSqlImport(this,'dev-instance','smoke_hybris_db','gs://sofology-qa-dev-bucket/smoke_hybris_db.sql.gz')
    }
}
stage('Sync media files'){
    container('cloudsdk'){
        cloudSdkLogin(this,<path to SA key file>)
        #syncBuckets(this,<source bucket>,<destination bucket>)
        syncBuckets(this,'gs://sofology-qa-dev-bucket','gs://sofology-smoke-dev-bucket')
    }
}
...
```
